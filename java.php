<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="php.css">
	
</head>
<body>
    <!-- header -->
	<header>
		<div class="container">
            <div class="navbar">
                <h1>Amaylia's</h1>
                <ul>
                    <li ><a href="index.html">HOME</a></li>
                    <li class="active"><a href="blog.html">BLOG</a></li>
                </ul>
            </div>
		</div>
	</header>

	<!-- banner -->
	<section class="banner">
        <h1>JavaScript</h1>
        <div class="isi">
            <img src="img/java.jpeg" class="foto">
        <p>adalah bahasa pemrograman yang digunakan dalam pengembangan website
			agar lebih dinamis dan interaktif. Dengan JavaScript kita bisa membuat aplikasi, tools atau game pada web<br></p>
        <p>Jika HTML dan CSS adalah yang memberikan elemen interaktif halaman web yang melibatkan pengguna</p>
			
		<p>Berikut kelebihan JavaScript : </p>
        <li>Bahasa yang paling populer</li>
        <li>Kemudahan Mempelajari </li>
        <li>Dinamnis</li>
		<li>Efisien</li>
		<br><br>

		<h1>Sejarah JavaScript</h1>	
		<p>JavaScript pertama kali diciptakan oleh Brendan Eich, seorang karyawan Netscape, pada tahun 1995. Netscape kala itu merupakan 
		perusahaan software ternama yang dikenal dengan web browser miliknya, Netscape Navigator. Brendan Eich pada awalnya diminta untuk membuat 
		bahasa scripting seperti Java namun dapat diterapkan untuk browser. Ia pun mendesain bahasa pemrograman baru dengan menggunakan fitur
		-fitur yang terinspirasi dari Java, Scheme, dan Self. <br><br>Pada saat itu, bahasa pemrograman yang ia buat diberi nama Mocha dan 
		hanya dibuat dalam waktu 10 hari karena waktu yang mepet dengan peluncuran Netscape Navigator versi 2. Mocha pun kemudian diaplikasikan 
		pada browser Netscape Navigator. Nama Mocha sempat mengalami perubahan menjadi LiveScript.<br><br>Netscape kemudian bekerja sama dengan
		Sun (saat ini Oracle) yang kala itu memegang lisensi Java. Kerja sama itu membawa Netscape mengubah nama LiveScript menjadi JavaScript 
		dengan alasan kebutuhan marketing. Pasalnya, pada saat itu Java populer sehingga cara ini dianggap dapat ikut menaikkan pamor JavaScript.
		Cara ini ternyata cukup berhasil. Saat itu JavaScript hanya dapat digunakan pada Netscape Navigator saja.<br><br>Tidak lama berselang, 
		Microsoft membuat bahasa baru yang disebut Jscript yang diaplikasikan pada browser Internet Explorer miliknya. Pada dasarnya JavaScript 
		adalah hasil adopsi dari JavaScript. Netscape akhirnya mencoba melakukan standarisasi terhadap JavaScript. Pada tahun 1997, JavaScript 
		berhasil diajukan ke ECMA Internasional dengan label ECMAScript.<br><br>Pada tahun-tahun berikutnya, ECMAScript mengalami banyak revisi dan 
		peningkatan sehingga JavaScript menjadi bahasa pemrograman yang dapat digunakan pada berbagai jenis aplikasi. Meski telah berkembang, 
		JavaScript juga sempat mengalami hambatan dalam implementasinya. Kala itu, JavaScript dinilai masih memiliki beberapa kekurangan fitur. 
		Namun pada tahun 2005, Jesse James Grrett membantu mengembalikan pamor JavaScript dengan mempopulerkan AJAX.<br><br>JavaScript kemudian 
		berkembang dengan pustaka baru yang kuat seperti JQuery dan MooTools yang mengurangi ketidakkonsistenan browser dan membuatnya lebih 
		mudah untuk diterapkan pada design patterns. ECMAScript versi 5 kemudian dirilis pada tahun 2009, dan terus dikembangkan hingga 
		JavaScript seperti sekarang.</p>
        </div>

        <br><p><h1>Berikut adalah contoh program dengan bahasa JavaScript : </h1><br></p>
		<p> <b>1. Form Validasi</b>
		<script language="javascript">
        function data(){
            var nama = document.getElementById("nama").value;
            var nim = document.getElementById("nim").value;
            var ttl = document.getElementById("ttl").value;
            var alamat = document.getElementById("alamat").value;
            var email = document.getElementById("email").value;
            if (nama!="" && nim!="" && ttl!="" && alamat!="" && email!="") {
                alert( 'Selamat datang ' +nama);
                return true;
            } else{
                alert('Harap isi dengan lengkap!');
            }
        }
    </script>

    <div class="form">
        <p><BR>Data Diri</p>
        <FORM NAME="fform">
            <PRE>
      Nama   :  <input type="text" size="11" name="nama" id="nama"><br><br>
      NIM    :  <input type="text" size="11" name="nim" id="nim"><br><br>
      TTL    :  <input type="date" size="11" name="ttl" id="ttl"><br><br>
      Alamat :  <input type="text" size="11" name="alamat" id="alamat"><br><br>
      Email  :  <input type="email" size="11" name="email" id="email"><br><br>

            </PRE>
            <P>
                <INPUT TYPE="button" value="Submit" onclick="data()">
                <INPUT TYPE="reset" value="Ulang">
        </FORM>
    </div>


		<p> <b>2. Form Operasi Penjumlahan</b>
		<P><SCRIPT language="JavaScript">
        function jumlah()
        {
            var bil1 = parseFloat(document.fform.bilangan1.value);
            if (isNaN (bil1))
            bil1=0.0;
            var bil2 = parseFloat(document.fform.bilangan2.value);
            if (isNaN (bil2))
            bil2=0.0;
			var bil3 = parseFloat(document.fform.bilangan3.value);
            if (isNaN (bil3))
            bil3=0.0;
			var bil4 = parseFloat(document.fform.bilangan4.value);
            if (isNaN (bil4))
            bil4=0.0;
			var bil5 = parseFloat(document.fform.bilangan5.value);
            if (isNaN (bil5))
            bil5=0.0;
            var hasil = bil1 + bil2 + bil3 + bil4 + bil5;
            alert ("Hasil Penjumlahan = " + hasil);
        }
    </SCRIPT></P>

    <FORM NAME ="fform">
            <PRE>
                <br>Bilangan Pertama :<input type="text" size="11" name="bilangan1">
				<br>Bilangan Kedua :<input type="text" size="11" name="bilangan2">
				<br>Bilangan Ketiga :<input type="text" size="11" name="bilangan3">
				<br>Bilangan Keempat :<input type="text" size="11" name="bilangan4">
				<br>Bilangan Kelima :<input type="text" size="11" name="bilangan5">
            
            </PRE>
            <P>
                <INPUT TYPE="button" value="Jumlahkan" onclick="jumlah()">
                <INPUT TYPE="reset" value="Ulang">
    </FORM>
	<p><b>3. Operasi Aritmatika dengan Bahasa JavaScript</b></p>
		<P><SCRIPT language="JavaScript">
    	    document.writeln("<PRE>");
			var A = 100; var B = 200; var C = 300; var D = 400; var E = A + B;
    	    document.writeln('100 + 200 = ' + E); E = B + C;
        	document.writeln('200 + 300 = ' + E); E = C + D;
    	    document.writeln('300 + 400 = ' + E);
        	document.writeln("<PRE>");
    </SCRIPT></P>

	</p><b>4. Perulangan For</b></p>
	<script language="JavaScript">
		for (let i = 0; i< 5; i++){
			document.write("<p>Pemrograman Web ke-"+i+"<br></p>");
		}
	</script>
</section>
<!-- about -->
	<section class="about">
		<div class="container">
			<h3>ABOUT</h3>
			<p>Hello Everyone!!<br>
			Perkenalkan nama saya Amaylia Ananda Cinta Naditya.<br> Saya lahir di Yogyakarta pada hari Minggu tahun 2003.
			Saat ini saya sedang menempuh pendidikan di Universitas Ahmad Dahlan </p>
		</div>
	</section>

	<!-- footer -->
	<footer>
		<div class="container">
			<small> Copyright &copy; 2021 - Amaylia Ananda, All Rights Reserved.</small>
		</div>
	</footer>
</body>
</html>