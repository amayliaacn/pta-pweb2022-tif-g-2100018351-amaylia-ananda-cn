<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="php.css">
	
</head>
<body>
    <!-- header -->
	<header>
		<div class="container">
            <div class="navbar">
                <h1>Amaylia's</h1>
                <ul>
                    <li ><a href="index.html">HOME</a></li>
                    <li class="active"><a href="blog.html">BLOG</a></li>
                </ul>
            </div>
		</div>
	</header>

	<!-- banner -->
	<section class="banner">
        <h1>PHP (Hypertext Preprocessor)</h1>
        <div class="isi">
            <img src="img/php.jpeg" class="foto">
        <p>adalah sebuah bahasa pemrograman server side scripting yang bersifat open source.<br></p>
        <p>Berikut kelebihan PHP : </p>
        <li>Mudah dipelajari</li>
        <li>Bersifat open source</li>
        <li>Banyak pilihan database</li>
        </div>

		<br><p><h1>Sejarah PHP</h1><p>
			<p>Pada awalnya PHP merupakan kependekan dari Personal Home Page (Situs personal).
			PHP pertama kali dibuat oleh Rasmus Lerdorf pada tahun 1995.pada waktu itu PHP masih bernama Form Interpreted (FI), yang wujudnya 
			berupa sekumpulan skrip yang digunakan ntuk mengolah data formulir dari web.<br><br>Selanjutnya Rasmus merilis kode sumber tersebut 
			untuk umum dan menamakannya PHP/FI. Dengan perilisan kode sumber ini menjadi sumber terbuka, maka banyak pemorgraman yang tertarik 
			untuk ikut mengambangkan PHP.<br><br>Pada November 1997, dirilis PHP/FI 2.0 . Pada rilis ini, interpreter PHP sudah diimplementasikan
			dalam program C. Dalam rilis ini disertakan juga modul - modul ekstensi yang meningkatkan kemampuam PHP/FI secara signifikan.<br><br>
			Pada tahun 1997, sebuah perusahaan bernama Zend menulis ulang interpreter PHP menjadi lebih bersih,lebih baik dan lebih cepat. 
			Kemudian pada Juni 1998, perusahaan tersebut merilis interpreter baru untuk PHP dan meresmikan rilis tersebut sebagai PHP 3.0 dan 
			singkatan PHP diubah menjadi akronim berulang PHP : Hypertext Preprocessing.<br><br>Pada pertengahan tahun 1999, Zend merilis 
			interpreter PHP baru dan rilis tersebut dikenal dengan PHP 4.0. PHP 4.0 adalah versi PHP yang paling banyak dipakai pada awal abad 
			ke-21. Versi ini banyak dipakai disebabkan kemampuannya untuk membangun aplikasi web kompleks tetapi tetap memiliki kecepatan dan 
			stabilitas yang tinggi.<br><br>Pada Juni 2004, Zend merilis PHP 5.0. Dalam versi ini, inti dari interpreter PHP mengalami perubahan 
			besar. Versi ini juga memasukkan model pemrograman berorientasi objek ke dalam PHP untuk menjawab perkembangan bahasa pemrograman 
			ke arah paradigma berorientasi objek. Server web bawaan ditambahkan pada versi 5.4 untuk mempermudah pengembang menjalankan kode 
			PHP tanpa menginstall software server.<br><br>Versi terbaru dan stabil dari bahasa pemograman PHP saat ini adalah versi 7.0.16 dan 7.1.2 
			yang resmi dirilis pada tanggal 17 Februari 2017.<p>
        <br><p><h1>Berikut adalah contoh program dengan bahasa PHP : </h1><br></p>
        <form id="form1" name="form1" method="post" action="">
			<?php
				echo "<b>1. Percabangan If Else</b><br>";
			?>
			<p>MASUKKAN NILAI : 0 - 100</p>
            <table>
                <tr>
                    <td>Masukan Nama</td>
                    <td>: <input type="text" name="nama" id="nama"> </td>
                </tr>
				<tr>
                    <td>Masukan NIM</td>
                    <td>: <input type="text" name="nim" id="nim"> </td>
                </tr>
				<tr>
                    <td>Masukan Kelas</td>
                    <td>: <input type="text" name="kelas" id="kelas"> </td>
                </tr>
				<tr>
                    <td>Masukan Mata Kuliah</td>
                    <td>: <input type="text" name="matkul" id="matkul"> </td>
                </tr>
				<tr>
                    <td>Masukan Nilai</td>
                    <td>: <input type="text" name="nilai" id="nilai"> </td>
                </tr>
                 	 <td colspan="2">
                        <br><br><input type="submit" name="submit" value="Submit" id="submit">
                        <input type="reset" name="reset" value="Reset" id="reset"><br><br>
                	</td>
                </tr>
            </table>
        </form>
<?php
if (isset($_POST['submit'])){
	$nama = $_POST['nama'];
	$nim = $_POST['nim'];
	$kelas = $_POST['kelas'];
	$matkul = $_POST['matkul'];
	$nilai = $_POST['nilai'];
	$nilaihuruf = $_POST['nilai'];
	$submit = $_POST['submit'];


	if(($submit < 0) || ($nilai > 100)){
		echo "Nilai yang anda masukkan salah!";
	}
    else{
		if (($nilai <= 100.00) && ($nilai >= 80.00)){
			$nilaihuruf = "A";
		}
		else if (($nilai <= 79.99) && ($nilai >= 70.00)){
			$nilaihuruf = "B";
		}
		else if (($nilai <= 69.99) && ($nilai >= 60.00)){
			$nilaihuruf = "C";
		}
		else if (($nilai <= 59.99) && ($nilai >= 50.00)){
			$nilaihuruf = "D";
		}
		else if (($nilai <= 49.99) && ($nilai > 0.0)){
			$nilaihuruf = "E";
		} 
		echo "= REKAP =<br>";
		echo "Nama			: $nama<br>";
		echo "NIM  \ Kelas	: $nim \ $kelas<br>";
		echo "Memperoleh nilai $nilaihuruf dari mata kuliah $matkul<br>"; 
    }
}
?>
<?php
echo "<br><b>2. Fungsi dengan Return dan Parameter</b><br>";
function kel_lingkaran($jari){
    return 2*3.14*$jari;
}

$r=14;
echo "Keliling Lingkaran dengan jari - jari $r = ";
echo kel_lingkaran($r);
echo "<br><br>"
?>

<?php
echo "<b>3. Mencari Elemen Array</b><br>";
$arrBuah=array("Belimbing" , "Apel" , "Durian" , "Sawo" , "Anggur");
echo"Buah : $arrBuah[0],$arrBuah[1],$arrBuah[2],$arrBuah[3],$arrBuah[4]<br>";
if(in_array("Kedondong",$arrBuah)){
    echo "<br>Ada buah kedondong di dalam array tersebut!";
}else{
    echo "<br>Tidak ada buah kedondong di array tersebut!";
}
?>
</section>

<!-- about -->
	<section class="about">
		<div class="container">
			<h3>ABOUT</h3>
			<p>Hello Everyone!!<br>
			Perkenalkan nama saya Amaylia Ananda Cinta Naditya.<br>
			Saya lahir di Yogyakarta pada hari Minggu tahun 2003.
			Saat ini saya sedang menempuh pendidikan di Universitas Ahmad Dahlan </p>
		</div>
	</section>

	<!-- footer -->
	<footer>
		<div class="container">
			<small> Copyright &copy; 2021 - Amaylia Ananda, All Rights Reserved.</small>
		</div>
	</footer>
</body>
</html>